# -*-coding:utf-8-*-
#!/usr/bin/env python
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import webapp2
import sys
import os
sys.path.insert(0, os.getcwd() + "\\lib")
from sys1 import *
from requests_toolbelt.adapters import appengine
appengine.monkeypatch()
from google.appengine.api import urlfetch
urlfetch.set_default_fetch_deadline(180)


class MainHandler(webapp2.RequestHandler):
    def get(self):
		from google.appengine.api import urlfetch
		urlfetch.set_default_fetch_deadline(1503616)
		country = self.request.get('country')
		country = (str)(country)
		budget = self.request.get('budget')
		budget = (int)(budget)
		country = country.upper()
		#adsetname = country + ' - 13 - 45' 
		biddingchoice = self.request.get('biddingchoice')
		biddingchoice = (str)(biddingchoice)
		campaignname  = self.request.get('campaignname')
		campaignname = (str)(campaignname)
		#adsetname = adsetname + '- ' + campaignid1
		if (biddingchoice=='m' or biddingchoice=='M'):
			#adsetname = adsetname + '- Manual'
			bidamount  = self.request.get('bidamount')
			bidamount = (int)(bidamount)
			#self.response.write("manual")
			createcampaign(campaignname,country,budget,bidamount)
		else:
			createcampaign(campaignname,country,budget)

app = webapp2.WSGIApplication([
    ('/test', MainHandler)
], debug=True)

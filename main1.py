# -*-coding:utf-8-*-
#!/usr/bin/env python
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import webapp2
import sys
import os
sys.path.insert(0, os.getcwd() + "\\lib")
#from sys1 import *
from querycode import *
from requests_toolbelt.adapters import appengine
appengine.monkeypatch()
from google.appengine.api import urlfetch
urlfetch.set_default_fetch_deadline(180)


class MainHandler(webapp2.RequestHandler):
    def get(self):
		from google.appengine.api import urlfetch
		urlfetch.set_default_fetch_deadline(1503616)
		bigqu()

app = webapp2.WSGIApplication([
    ('/bigquery', MainHandler)
], debug=True)
